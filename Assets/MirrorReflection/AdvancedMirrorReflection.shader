Shader "AdvancedMirrorReflection"
{
	Properties 
	{
_FresnelCurve("_FresnelCurve", Range(0,1) ) = 1
_ReflMult("_ReflMult", Range(0,1) ) = 1
_Color("Main Color", Color) = (1,1,1,1)
_MainTexture("_MainTexture", 2D) = "black" {}
_Normal("_Normal", 2D) = "bump" {}
_ReflectionTex("_ReflectionTex", 2D) = "black" {}
_Specular("_Specular", Color) = (1,1,1,1)
_SpecularLevel("_SpecularLevel", Range(0,1) ) = 0.422
_Glossnes("_Glossnes", Range(0.02,1) ) = 0.02
_NormalMult("_NormalMult", Range(0,1) ) = 0.5
_DiffuseEmission("Diffuse <-> Emission", Range(0,1) ) = 0.5

	}
	
	SubShader 
	{
		Tags
		{
"Queue"="Geometry"
"IgnoreProjector"="False"
"RenderType"="Opaque"

		}

		
Cull Back
ZWrite On
ZTest LEqual
ColorMask RGBA
Fog{
}


		CGPROGRAM
#pragma surface surf BlinnPhongEditor  vertex:vert
#pragma target 3.0


float _FresnelCurve;
float _ReflMult;
float4 _Color;
sampler2D _MainTexture;
sampler2D _Normal;
sampler2D _ReflectionTex;
float4 _Specular;
float _SpecularLevel;
float _Glossnes;
float _NormalMult;
float _DiffuseEmission;

			struct EditorSurfaceOutput {
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half3 Gloss;
				half Specular;
				half Alpha;
				half4 Custom;
			};
			
			inline half4 LightingBlinnPhongEditor_PrePass (EditorSurfaceOutput s, half4 light)
			{
half3 spec = light.a * s.Gloss;
half4 c;
c.rgb = (s.Albedo * light.rgb + light.rgb * spec);
c.a = s.Alpha;
return c;

			}

			inline half4 LightingBlinnPhongEditor (EditorSurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
			{
				half3 h = normalize (lightDir + viewDir);
				
				half diff = max (0, dot ( lightDir, s.Normal ));
				
				float nh = max (0, dot (s.Normal, h));
				float spec = pow (nh, s.Specular*128.0);
				
				half4 res;
				res.rgb = _LightColor0.rgb * diff;
				res.w = spec * Luminance (_LightColor0.rgb);
				res *= atten * 2.0;

				return LightingBlinnPhongEditor_PrePass( s, res );
			}
			
			struct Input {
				float3 viewDir;
float2 uv_Normal;
float4 screenPos;
float2 uv_MainTexture;

			};

			void vert (inout appdata_full v, out Input o) {
float4 VertexOutputMaster0_0_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_1_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_2_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_3_NoInput = float4(0,0,0,0);


			}
			

			void surf (Input IN, inout EditorSurfaceOutput o) {
				o.Normal = float3(0.0,0.0,1.0);
				o.Alpha = 1.0;
				o.Albedo = 0.0;
				o.Emission = 0.0;
				o.Gloss = 0.0;
				o.Specular = 0.0;
				o.Custom = 0.0;
				
float4 Invert2= float4(1.0, 1.0, 1.0, 1.0) - _DiffuseEmission.xxxx;
float4 Tex2DNormal0=float4(UnpackNormal( tex2D(_Normal,(IN.uv_Normal.xyxy).xy)).xyz, 1.0 );
float4 Fresnel0=(1.0 - dot( normalize( float4( IN.viewDir.x, IN.viewDir.y,IN.viewDir.z,1.0 ).xyz), normalize( Tex2DNormal0.xyz ) )).xxxx;
float4 Invert1= float4(1.0, 1.0, 1.0, 1.0) - Fresnel0;
float4 Multiply4=_FresnelCurve.xxxx * Invert1;
float4 Add1=Multiply4 + Fresnel0;
float4 Multiply7=Fresnel0 * _NormalMult.xxxx;
float4 Multiply6=Tex2DNormal0 * Multiply7;
float4 Reflect0=reflect(((IN.screenPos.xy/IN.screenPos.w).xyxy),Multiply6);
float4 Tex2D1=tex2D(_ReflectionTex,Reflect0.xy);
float4 Multiply0=Tex2D1 * _ReflMult.xxxx;
float4 Multiply2=Add1 * Multiply0;
float4 Tex2D0=tex2D(_MainTexture,(IN.uv_MainTexture.xyxy).xy);
float4 Multiply5=_Color * Tex2D0;
float4 Invert0= float4(1.0, 1.0, 1.0, 1.0) - Multiply2;
float4 Multiply1=Multiply5 * Invert0;
float4 Add0=Multiply2 + Multiply1;
float4 Multiply8=Invert2 * Add0;
float4 Multiply9=_DiffuseEmission.xxxx * Add0;
float4 Multiply3=_SpecularLevel.xxxx * _Specular;
float4 Master0_5_NoInput = float4(1,1,1,1);
float4 Master0_7_NoInput = float4(0,0,0,0);
float4 Master0_6_NoInput = float4(1,1,1,1);
o.Albedo = Multiply8;
o.Normal = Tex2DNormal0;
o.Emission = Multiply9;
o.Specular = _Glossnes.xxxx;
o.Gloss = Multiply3;

				o.Normal = normalize(o.Normal);
			}
		ENDCG
	}
	Fallback "Diffuse"
}