﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
    public int speed = 2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, -1 * speed * Time.deltaTime, 0);
	}
}
